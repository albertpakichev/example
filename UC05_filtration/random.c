void *randomstring(size_t length, char *name) {

    static char charset[] = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789@.-_";        
    char *randomString = NULL;
	srand(time(0));
	
    if (length) {
        randomString = malloc(sizeof(char) * (length +1));

        if (randomString) {
			int n;        	
            for (n = 0;n < length;n++) {            
                int key = rand() % (int)(sizeof(charset) -1);
                randomString[n] = charset[key];
            }

            randomString[length] = '\0';
        }
    }	

	lr_save_string(randomString, name);

    free(randomString);
}