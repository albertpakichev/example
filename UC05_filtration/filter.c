void random_filter(void){
	
	char encoded_query[200];
	char *filter2, *value2;
	srand(time(0)); 
	
	switch(rand()%7){
		case 0:
			lr_save_string("q", "filter1");
			filter2 = "search_string";
			randomstring(rand()%15, "value1");
			value2 = lr_eval_string("{value1}");
			sprintf(encoded_query, "{\"filtering\": {}, \"filtering_or\": {}, \"sorting\": \"created\", \"sortreverse\": null, \"search_string\": \"%s\"}", value2);
			break;
		case 1:
			lr_save_string("date_from", "filter1");
			filter2 = "created__gte";
			lr_save_datetime("%Y-%m-%d", DATE_NOW - ONE_DAY*(rand()%360 + 1), "value1");
			value2 = lr_eval_string("{value1}");
			sprintf(encoded_query, "{\"filtering\": {\"created__lte\": \"%s\"}, \"filtering_or\": {}, \"sorting\": \"created\", \"sortreverse\": null, \"search_string\": \"\"}", value2);
			break;
		case 2:
			lr_save_string("date_to", "filter1");
			filter2 = "created__lte";
			lr_save_datetime("%Y-%m-%d", DATE_NOW + ONE_DAY*(rand()%360 + 1), "value1");
			value2 = lr_eval_string("{value1}");
			sprintf(encoded_query, "{\"filtering\": {\"created__gte\": \"%s\"}, \"filtering_or\": {}, \"sorting\": \"created\", \"sortreverse\": null, \"search_string\": \"\"}", value2);
			break;
		case 3:
			lr_save_string("queue", "filter1");
			filter2 = "queue__id__in";
			lr_param_sprintf("value1", "%d", rand()%2 + 1);
			value2 = lr_eval_string("{value1}");
			sprintf(encoded_query, "{\"filtering\": {\"queue__id__in\": [%s]}, \"filtering_or\": {\"queue__id__in\": [%s]}, \"sorting\": \"created\", \"sortreverse\": null, \"search_string\": \"\"}", value2, value2);
			break;
		case 4:
			lr_save_string("status", "filter1");
			filter2 = "status__in";
			lr_param_sprintf("value1", "%d", rand()%5 + 1);
			value2 = lr_eval_string("{value1}");
			sprintf(encoded_query, "{\"filtering\": {\"status__in\": [%s]}, \"filtering_or\": {\"status__in\": [%s]}, \"sorting\": \"created\", \"sortreverse\": null, \"search_string\": \"\"}", value2, value2);
			break;
		case 5:
			lr_save_string("kbitem", "filter1");
			filter2 = "kbitem__in";
			switch(rand()%3){
				case 0:
					value2 = "-1";
					lr_save_string(value2, "value1");
					sprintf(encoded_query, "{\"filtering\": {\"kbitem__in\": [%s]}, \"filtering_or\": {\"kbitem__isnull\": true}, \"sorting\": \"created\", \"sortreverse\": null, \"search_string\": \"\"}", value2);
					break;
				case 1:
					value2 = "1";
					lr_save_string(value2, "value1");
					sprintf(encoded_query, "{\"filtering\": {\"kbitem__in\": [%s]}, \"filtering_or\": {\"kbitem__in\": [%s]}, \"sorting\": \"created\", \"sortreverse\": null, \"search_string\": \"\"}", value2, value2);
					break;
				case 2:
					value2 = "2";
					lr_save_string(value2, "value1");
					sprintf(encoded_query, "{\"filtering\": {\"kbitem__in\": [%s]}, \"filtering_or\": {\"kbitem__in\": [%s]}, \"sorting\": \"created\", \"sortreverse\": null, \"search_string\": \"\"}", value2, value2);
					break;
				case 3:
					value2 = "3";
					lr_save_string(value2, "value1");
					sprintf(encoded_query, "{\"filtering\": {\"kbitem__in\": [%s]}, \"filtering_or\": {\"kbitem__in\": [%s]}, \"sorting\": \"created\", \"sortreverse\": null, \"search_string\": \"\"}", value2, value2);
					break;					
			}
			break;	
		case 6:
			lr_save_string("assigned_to", "filter1");
			value2 = lr_eval_string("{id_user}");
			lr_save_string(value2, "value1");
			if (atoi(value2)==-1) {
				sprintf(encoded_query, "{\"filtering\": {\"assigned_to__id__in\": [-1]}, \"filtering_or\": {\"assigned_to__id__isnull\": true}, \"sorting\": \"created\", \"sortreverse\": null, \"search_string\": \"\"}");
			}
			else {
				sprintf(encoded_query, "{\"filtering\": {\"assigned_to__id__in\": [%s]}, \"filtering_or\": {\"assigned_to__id__in\": [%s]}, \"sorting\": \"created\", \"sortreverse\": null, \"search_string\": \"\"}", value2, value2);
			}
			break;			
	}
		
	lr_load_dll("micsocket.dll");
	
	lr_save_string(util_base64encode(encoded_query), "query" );
}

	