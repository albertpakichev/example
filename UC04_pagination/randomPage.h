void random_start(char *record_total, char *param_name)
{
	int rand_start;
	
	srand(time(0));
	
	if (atoi(record_total)%10 == 0)
	{
		rand_start = (rand()%(atoi(record_total)/10))*10;
	}
	else
	{
		rand_start = (rand()%(atoi(record_total)/10 + 1))*10;
	}
	
	lr_param_sprintf (param_name, "%d", rand_start);
}