Action()
{
	
	lr_start_transaction("UC06_TR01_main_page");
	
	web_url("{host}:{port}", 
		"URL=http://{host}:{port}/", 
		"TargetFrame=", 
		"Resource=0", 
		"RecContentType=text/html", 
		"Referer=", 
		"Snapshot=t1.inf", 
		"Mode=HTML", 
		LAST);

	lr_end_transaction("UC06_TR01_main_page",LR_AUTO);

	lr_think_time(5);
	
	web_reg_save_param_regexp(
      "ParamName=csrfmiddlewaretoken",
      "RegExp=<input type=\\\"hidden\\\" name=\\\"csrfmiddlewaretoken\\\" value=\\\"(.*?)\\\">",
      "Ordinal=1",
    SEARCH_FILTERS,
    LAST);

	lr_start_transaction("UC06_TR02_login_page");

	web_url("Log In", 
		"URL=http://{host}:{port}/login/?next=/", 
		"TargetFrame=", 
		"Resource=0", 
		"RecContentType=text/html", 
		"Referer=http://{host}:{port}/", 
		"Snapshot=t7.inf", 
		"Mode=HTML", 
		LAST);

	lr_end_transaction("UC06_TR02_login_page",LR_AUTO);

	web_reg_save_param_regexp(
	    "ParamName=query_encoded",
	    "RegExp=name='query_encoded' value='(.*?)'/>",
	    SEARCH_FILTERS,
	    LAST);
	
	lr_think_time(5);
	
	lr_start_transaction("UC06_TR03_login");

	web_submit_data("login", 
		"Action=http://{host}:{port}/login/", 
		"Method=POST", 
		"TargetFrame=", 
		"RecContentType=text/html", 
		"Referer=http://{host}:{port}/login/?next=/", 
		"Snapshot=t8.inf", 
		"Mode=HTML", 
		ITEMDATA, 
		"Name=username", "Value={username}", ENDITEM, 
		"Name=password", "Value={password}", ENDITEM, 
		"Name=next", "Value=/", ENDITEM, 
		"Name=csrfmiddlewaretoken", "Value={csrfmiddlewaretoken}", ENDITEM, 
		LAST);

	lr_end_transaction("UC06_TR03_login",LR_AUTO);
	
	lr_think_time(5);	
	
	web_reg_find("Text=Your username and password didn't match. Please try again.", "Fail=Found", LAST);

	web_reg_save_param_regexp(
		"ParamName=id_ticket",
		"RegExp=\"ticket\": \"(.+?) ",
		"Ordinal=all",
		"Group=1",
		SEARCH_FILTERS,
		LAST);
	
	lr_start_transaction("UC06_TR04_page_queryEncoded");
	
	web_url("{query_encoded}", 
		"URL=http://{host}:{port}/datatables_ticket_list/{query_encoded}?draw=1&columns%5B0%5D%5Bdata%5D=id&columns%5B0%5D%5Bname%5D=&columns%5B0%5D%5Bsearchable%5D=true&columns%5B0%5D%5Borderable%5D=false&columns%5B0%5D%5Bsearch%5D%5Bvalue%5D=&columns%5B0%5D%5Bsearch%5D%5Bregex%5D=false&columns%5B1%5D%5Bdata%5D=ticket&columns%5B1%5D%5Bname%5D=&columns%5B1%5D%5Bsearchable%5D=true&"
		"columns%5B1%5D%5Borderable%5D=true&columns%5B1%5D%5Bsearch%5D%5Bvalue%5D=&columns%5B1%5D%5Bsearch%5D%5Bregex%5D=false&columns%5B2%5D%5Bdata%5D=priority&columns%5B2%5D%5Bname%5D=&columns%5B2%5D%5Bsearchable%5D=true&columns%5B2%5D%5Borderable%5D=true&columns%5B2%5D%5Bsearch%5D%5Bvalue%5D=&columns%5B2%5D%5Bsearch%5D%5Bregex%5D=false&columns%5B3%5D%5Bdata%5D=queue&columns%5B3%5D%5Bname%5D=&columns%5B3%5D%5Bsearchable%5D=true&columns%5B3%5D%5Borderable%5D=true&columns%5B3%5D%5Bsearch%5D%5Bvalue%5D=&"
		"columns%5B3%5D%5Bsearch%5D%5Bregex%5D=false&columns%5B4%5D%5Bdata%5D=status&columns%5B4%5D%5Bname%5D=&columns%5B4%5D%5Bsearchable%5D=true&columns%5B4%5D%5Borderable%5D=true&columns%5B4%5D%5Bsearch%5D%5Bvalue%5D=&columns%5B4%5D%5Bsearch%5D%5Bregex%5D=false&columns%5B5%5D%5Bdata%5D=created&columns%5B5%5D%5Bname%5D=&columns%5B5%5D%5Bsearchable%5D=true&columns%5B5%5D%5Borderable%5D=true&columns%5B5%5D%5Bsearch%5D%5Bvalue%5D=&columns%5B5%5D%5Bsearch%5D%5Bregex%5D=false&columns%5B6%5D%5Bdata%5D=due_date"
		"&columns%5B6%5D%5Bname%5D=&columns%5B6%5D%5Bsearchable%5D=true&columns%5B6%5D%5Borderable%5D=true&columns%5B6%5D%5Bsearch%5D%5Bvalue%5D=&columns%5B6%5D%5Bsearch%5D%5Bregex%5D=false&columns%5B7%5D%5Bdata%5D=assigned_to&columns%5B7%5D%5Bname%5D=&columns%5B7%5D%5Bsearchable%5D=true&columns%5B7%5D%5Borderable%5D=true&columns%5B7%5D%5Bsearch%5D%5Bvalue%5D=&columns%5B7%5D%5Bsearch%5D%5Bregex%5D=false&columns%5B8%5D%5Bdata%5D=submitter&columns%5B8%5D%5Bname%5D=&columns%5B8%5D%5Bsearchable%5D=true&"
		"columns%5B8%5D%5Borderable%5D=true&columns%5B8%5D%5Bsearch%5D%5Bvalue%5D=&columns%5B8%5D%5Bsearch%5D%5Bregex%5D=false&columns%5B9%5D%5Bdata%5D=time_spent&columns%5B9%5D%5Bname%5D=&columns%5B9%5D%5Bsearchable%5D=true&columns%5B9%5D%5Borderable%5D=true&columns%5B9%5D%5Bsearch%5D%5Bvalue%5D=&columns%5B9%5D%5Bsearch%5D%5Bregex%5D=false&columns%5B10%5D%5Bdata%5D=kbitem&columns%5B10%5D%5Bname%5D=&columns%5B10%5D%5Bsearchable%5D=true&columns%5B10%5D%5Borderable%5D=true&"
		"columns%5B10%5D%5Bsearch%5D%5Bvalue%5D=&columns%5B10%5D%5Bsearch%5D%5Bregex%5D=false&order%5B0%5D%5Bcolumn%5D=0&order%5B0%5D%5Bdir%5D=asc&start=0&length=25&search%5Bvalue%5D=&search%5Bregex%5D=false&_=1666636427323", 
		"TargetFrame=", 
		"Resource=0", 
		"RecContentType=application/json", 
		"Referer=http://{host}:{port}/tickets/", 
		"Snapshot=t9.inf", 
		"Mode=HTML", 
		LAST);

	lr_end_transaction("UC06_TR04_page_queryEncoded",LR_AUTO);

	lr_think_time(5);
	
	lr_save_string(lr_paramarr_random("id_ticket"), "id_tickets");

//	web_reg_save_param_ex(
//		"ParamName=Unassigned",
//		"LB=unassigned/' type='application/rss+xml' title='",
//		"RB=' />",
//		"NotFound=warning",
//		SEARCH_FILTERS,
//		"Scope=Body",
//		LAST);

	
	web_reg_save_param_regexp(
		"ParamName=Unassigned",
		"RegExp=<th class=\\\"table-active\\\">Assigned To</th>\r\n                        <td>(.*?) <strong>",
		"NotFound=warning",
		SEARCH_FILTERS,
		LAST);

	
	lr_start_transaction("UC06_TR05_open_random_ticket");

	web_url("{id_tickets}", 
		"URL=http://{host}:{port}/tickets/{id_tickets}/", 
		"TargetFrame=", 
		"Resource=0", 
		"RecContentType=text/html", 
		"Referer=http://{host}:{port}/tickets/", 
		"Snapshot=t5.inf", 
		"Mode=HTML", 
		LAST);

	lr_end_transaction("UC06_TR05_open_random_ticket",LR_AUTO);
	
	lr_think_time(5);
	
	if (strcmp((lr_eval_string("{Unassigned}")),(lr_eval_string("{parameter}")))==0) {
    
    
	
	lr_start_transaction("UC06_TR06_ticket_assigned");

	web_url("{id_tickets}_2", 
		"URL=http://{host}:{port}/tickets/{id_tickets}/?take", 
		"TargetFrame=", 
		"Resource=0", 
		"RecContentType=text/html", 
		"Referer=http://{host}:{port}/tickets/{id_tickets}/", 
		"Snapshot=t6.inf", 
		"Mode=HTML", 
		LAST);

	
	
	lr_end_transaction("UC06_TR06_ticket_assigned",LR_AUTO);

	}

	lr_start_transaction("UC06_TR07_logout");

	web_url("Logout", 
		"URL=http://{host}:{port}/logout/", 
		"TargetFrame=", 
		"Resource=0", 
		"RecContentType=text/html", 
		"Referer=http://{host}:{port}/tickets/{id_tickets}/", 
		"Snapshot=t12.inf", 
		"Mode=HTML", 
		LAST);

	lr_end_transaction("UC06_TR07_logout",LR_AUTO);

	return 0;
}