#include "globals.h"
#include <time.h>
#include <string.h>
#define N 20
#define SET "QWERTYUIOPASDFGHJKLZXCVBNMqwertyuiopasdfghjklzxcvbnm1234567890"

void *randstring2()
{
   char s[N];
   int i, set_len;
  // srand(time(NULL));
   set_len = strlen(SET);
   for (i = 0; i < N - 1; i++)
      s[i] = SET[rand() % set_len];
   s[i] = '\0';
   
   lr_save_string(s, "rand_password");
   
   //puts(s);
   return 0;
}