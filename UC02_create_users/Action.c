Action()
{
	randstring2();
	randstring();
	
	lr_start_transaction("UC02_TR01_main_page");
	
	web_url("{host}:{port}", 
		"URL=http://{host}:{port}/", 
		"TargetFrame=", 
		"Resource=0", 
		"RecContentType=text/html", 
		"Referer=", 
		"Snapshot=t13.inf", 
		"Mode=HTML", 
		LAST);

	lr_end_transaction("UC02_TR01_main_page",LR_AUTO);

	lr_think_time(5);
	
	web_reg_save_param_regexp(
      "ParamName=csrfmiddlewaretoken",
      "RegExp=<input type=\\\"hidden\\\" name=\\\"csrfmiddlewaretoken\\\" value=\\\"(.*?)\\\">",
      "Ordinal=1",
    SEARCH_FILTERS,
    LAST);
	

	lr_start_transaction("UC02_TR02_login_page");

	web_url("Log In", 
		"URL=http://{host}:{port}/login/?next=/", 
		"TargetFrame=", 
		"Resource=0", 
		"RecContentType=text/html", 
		"Referer=http://{host}:{port}/", 
		"Snapshot=t19.inf", 
		"Mode=HTML", 
		LAST);
	
	lr_end_transaction("UC02_TR02_login_page",LR_AUTO);

	web_reg_save_param_regexp(
		"ParamName=query_encoded",
		"RegExp=name='query_encoded' value='(.*?)'/>",
		SEARCH_FILTERS,
		LAST);
	
	lr_think_time(5);
	
	lr_start_transaction("UC02_TR03_login");
	
	web_submit_data("login", 
		"Action=http://{host}:{port}/login/", 
		"Method=POST", 
		"TargetFrame=", 
		"RecContentType=text/html", 
		"Referer=http://{host}:{port}/login/?next=/", 
		"Snapshot=t20.inf", 
		"Mode=HTML", 
		ITEMDATA, 
		"Name=username", "Value={username}", ENDITEM, 
		"Name=password", "Value={password}", ENDITEM, 
		"Name=next", "Value=/", ENDITEM, 
		"Name=csrfmiddlewaretoken", "Value={csrfmiddlewaretoken}", ENDITEM, 
		LAST);

	lr_end_transaction("UC02_TR03_login",LR_AUTO);
	
	web_add_header("X-Requested-With", 
		"XMLHttpRequest");
	
	lr_think_time(5);
	
	lr_start_transaction("UC02_TR04_page_queryEncoded");
	
	web_url("{query_encoded}", 
		"URL=http://{host}:{port}/datatables_ticket_list/{query_encoded}?draw=1&columns%5B0%5D%5Bdata%5D=id&columns%5B0%5D%5Bname%5D=&columns%5B0%5D%5Bsearchable%5D=true&columns%5B0%5D%5Borderable%5D=false&columns%5B0%5D%5Bsearch%5D%5Bvalue%5D=&columns%5B0%5D%5Bsearch%5D%5Bregex%5D=false&columns%5B1%5D%5Bdata%5D=ticket&columns%5B1%5D%5Bname%5D=&columns%5B1%5D%5Bsearchable%5D=true&"
		"columns%5B1%5D%5Borderable%5D=true&columns%5B1%5D%5Bsearch%5D%5Bvalue%5D=&columns%5B1%5D%5Bsearch%5D%5Bregex%5D=false&columns%5B2%5D%5Bdata%5D=priority&columns%5B2%5D%5Bname%5D=&columns%5B2%5D%5Bsearchable%5D=true&columns%5B2%5D%5Borderable%5D=true&columns%5B2%5D%5Bsearch%5D%5Bvalue%5D=&columns%5B2%5D%5Bsearch%5D%5Bregex%5D=false&columns%5B3%5D%5Bdata%5D=queue&columns%5B3%5D%5Bname%5D=&columns%5B3%5D%5Bsearchable%5D=true&columns%5B3%5D%5Borderable%5D=true&columns%5B3%5D%5Bsearch%5D%5Bvalue%5D=&"
		"columns%5B3%5D%5Bsearch%5D%5Bregex%5D=false&columns%5B4%5D%5Bdata%5D=status&columns%5B4%5D%5Bname%5D=&columns%5B4%5D%5Bsearchable%5D=true&columns%5B4%5D%5Borderable%5D=true&columns%5B4%5D%5Bsearch%5D%5Bvalue%5D=&columns%5B4%5D%5Bsearch%5D%5Bregex%5D=false&columns%5B5%5D%5Bdata%5D=created&columns%5B5%5D%5Bname%5D=&columns%5B5%5D%5Bsearchable%5D=true&columns%5B5%5D%5Borderable%5D=true&columns%5B5%5D%5Bsearch%5D%5Bvalue%5D=&columns%5B5%5D%5Bsearch%5D%5Bregex%5D=false&columns%5B6%5D%5Bdata%5D=due_date"
		"&columns%5B6%5D%5Bname%5D=&columns%5B6%5D%5Bsearchable%5D=true&columns%5B6%5D%5Borderable%5D=true&columns%5B6%5D%5Bsearch%5D%5Bvalue%5D=&columns%5B6%5D%5Bsearch%5D%5Bregex%5D=false&columns%5B7%5D%5Bdata%5D=assigned_to&columns%5B7%5D%5Bname%5D=&columns%5B7%5D%5Bsearchable%5D=true&columns%5B7%5D%5Borderable%5D=true&columns%5B7%5D%5Bsearch%5D%5Bvalue%5D=&columns%5B7%5D%5Bsearch%5D%5Bregex%5D=false&columns%5B8%5D%5Bdata%5D=submitter&columns%5B8%5D%5Bname%5D=&columns%5B8%5D%5Bsearchable%5D=true&"
		"columns%5B8%5D%5Borderable%5D=true&columns%5B8%5D%5Bsearch%5D%5Bvalue%5D=&columns%5B8%5D%5Bsearch%5D%5Bregex%5D=false&columns%5B9%5D%5Bdata%5D=time_spent&columns%5B9%5D%5Bname%5D=&columns%5B9%5D%5Bsearchable%5D=true&columns%5B9%5D%5Borderable%5D=true&columns%5B9%5D%5Bsearch%5D%5Bvalue%5D=&columns%5B9%5D%5Bsearch%5D%5Bregex%5D=false&columns%5B10%5D%5Bdata%5D=kbitem&columns%5B10%5D%5Bname%5D=&columns%5B10%5D%5Bsearchable%5D=true&columns%5B10%5D%5Borderable%5D=true&"
		"columns%5B10%5D%5Bsearch%5D%5Bvalue%5D=&columns%5B10%5D%5Bsearch%5D%5Bregex%5D=false&order%5B0%5D%5Bcolumn%5D=0&order%5B0%5D%5Bdir%5D=asc&start=0&length=25&search%5Bvalue%5D=&search%5Bregex%5D=false&_=1666183241500", 
		"TargetFrame=", 
		"Resource=0", 
		"RecContentType=application/json", 
		"Referer=http://{host}:{port}/tickets/", 
		"Snapshot=t21.inf", 
		"Mode=HTML", 
		LAST);

	lr_end_transaction("UC02_TR04_page_queryEncoded",LR_AUTO);
	
	web_reg_find("Text=Your username and password didn't match. Please try again.", "Fail=Found", LAST);
	

	lr_think_time(5);

	lr_start_transaction("UC02_TR05_system_settings");

	web_url("system_settings", 
		"URL=http://{host}:{port}/system_settings/", 
		"TargetFrame=", 
		"Resource=0", 
		"RecContentType=text/html", 
		"Referer=http://{host}:{port}/tickets/", 
		"Snapshot=t22.inf", 
		"Mode=HTML", 
		LAST);

	lr_end_transaction("UC02_TR05_system_settings",LR_AUTO);

	lr_think_time(5);

	lr_start_transaction("UC02_TR06_maintain_users");
	
	web_url("Maintain Users", 
		"URL=http://{host}:{port}/{username}/auth/user/", 
		"TargetFrame=", 
		"Resource=0", 
		"RecContentType=text/html", 
		"Referer=http://{host}:{port}/system_settings/", 
		"Snapshot=t23.inf", 
		"Mode=HTML", 
		LAST);

	lr_end_transaction("UC02_TR06_maintain_users",LR_AUTO);
	
	
	lr_think_time(5);
	
	web_reg_save_param_regexp(
      "ParamName=csrfmiddlewaretoken1",
      "RegExp=<input type=\\\"hidden\\\" name=\\\"csrfmiddlewaretoken\\\" value=\\\"(.*?)\\\">",
      "Ordinal=1",
    SEARCH_FILTERS,
    LAST);

	lr_start_transaction("UC02_TR07_page_add");
	
	web_url("Add", 
		"URL=http://{host}:{port}/{username}/auth/user/add/", 
		"TargetFrame=", 
		"Resource=0", 
		"RecContentType=text/html", 
		"Referer=http://{host}:{port}/{username}/auth/user/", 
		"Snapshot=t31.inf", 
		"Mode=HTML", 
		LAST);
	
	lr_end_transaction("UC02_TR07_page_add",LR_AUTO);

	web_reg_save_param_regexp(
		"ParamName=id",
		"RegExp=/user/(.*)/ch",
		"Group=1",
		SEARCH_FILTERS,
		LAST);
	
	lr_think_time(5);
	
	lr_start_transaction("UC02_TR08_add");
	
	web_submit_data("add",
		"Action=http://{host}:{port}/admin/auth/user/add/",
		"Method=POST",
		"TargetFrame=",
		"RecContentType=text/html",
		"Referer=http://{host}:{port}/admin/auth/user/add/",
		"Snapshot=t67.inf",
		"Mode=HTML",
		ITEMDATA,
		"Name=csrfmiddlewaretoken", "Value={csrfmiddlewaretoken1}", ENDITEM,
		"Name=username", "Value={rand_username}", ENDITEM,
		"Name=password1", "Value={rand_password}", ENDITEM,
		"Name=password2", "Value={rand_password}", ENDITEM,
		"Name=_save", "Value=Save", ENDITEM,
		EXTRARES,
		"URL=/static/admin/img/icon-deletelink.svg", "Referer=http://{host}:{port}/admin/auth/user/{id}/change/", ENDITEM,
		LAST);


	lr_end_transaction("UC02_TR08_add",LR_AUTO);
	
	web_reg_save_param_regexp(
      "ParamName=csrfmiddlewaretoken2",
      "RegExp=<input type=\\\"hidden\\\" name=\\\"csrfmiddlewaretoken\\\" value=\\\"(.*?)\\\">",
      "Ordinal=1",
  SEARCH_FILTERS,
  LAST);
	
	lr_think_time(5);
	
	lr_start_transaction("UC02_TR09_User_Log");

	web_url("{username}", 
		"URL=http://{host}:{port}/admin/auth/user/{id}/change/", 
		"TargetFrame=", 
		"Resource=0", 
		"RecContentType=text/html", 
		"Referer=http://{host}:{port}/admin/auth/user/add/", 
		"Snapshot=t68.inf", 
		"Mode=HTML", 
		EXTRARES, 
		LAST);
	
	lr_end_transaction("UC02_TR09_User_Log",LR_AUTO);
	
	web_reg_find("Text=Staff status",
        LAST );
	
	lr_think_time(5);

	lr_start_transaction("UC02_TR10_change");

	web_submit_data("change", 
		"Action=http://{host}:{port}/{username}/auth/user/{id}/change/", 
		"Method=POST", 
		"TargetFrame=", 
		"RecContentType=text/html", 
		"Referer=http://{host}:{port}/{username}/auth/user/{id}/change/", 
		"Snapshot=t35.inf", 
		"Mode=HTML", 
		ITEMDATA, 
		"Name=csrfmiddlewaretoken", "Value={csrfmiddlewaretoken2}", ENDITEM, 
		"Name=username", "Value={rand_username}", ENDITEM, 
		"Name=first_name", "Value=", ENDITEM, 
		"Name=last_name", "Value=", ENDITEM, 
		"Name=email", "Value=", ENDITEM, 
		"Name=is_active", "Value=on", ENDITEM, 
		"Name=is_staff", "Value=on", ENDITEM, 
		"Name=last_login_0", "Value=", ENDITEM, 
		"Name=last_login_1", "Value=", ENDITEM, 
		"Name=date_joined_0", "Value={Data}", ENDITEM, 
		"Name=date_joined_1", "Value={Time}", ENDITEM, 
		"Name=initial-date_joined_0", "Value={Data}", ENDITEM, 
		"Name=initial-date_joined_1", "Value={Time}", ENDITEM, 
		"Name=_save", "Value=Save", ENDITEM, 
		LAST);

	lr_end_transaction("UC02_TR10_change",LR_AUTO);

	write(lr_eval_string("{rand_username}"), lr_eval_string("{rand_password}"), lr_eval_string("{id}"));
	
	lr_think_time(5);

	lr_start_transaction("UC02_TR11_logout");

	web_url("Log out", 
		"URL=http://{host}:{port}/{username}/logout/", 
		"TargetFrame=", 
		"Resource=0", 
		"RecContentType=text/html", 
		"Referer=http://{host}:{port}/{username}/auth/user/", 
		"Snapshot=t36.inf", 
		"Mode=HTML", 
		LAST);

	lr_end_transaction("UC02_TR11_logout",LR_AUTO);

	return 0;
}